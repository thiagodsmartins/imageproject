//
//  ImageModel.swift
//  ProjectImgurList
//
//  Created by Thiago dos Santos Martins on 16/03/22.
//

import Foundation

struct ImageModel: Decodable {
    var id: String?
    var author: String?
    var width: Int?
    var height: Int?
    var url: String?
    var download_url: String?
}

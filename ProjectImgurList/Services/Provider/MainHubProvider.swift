//
//  MainHubProvider.swift
//  ProjectImgurList
//
//  Created by Thiago dos Santos Martins on 16/03/22.
//

import Foundation
import Alamofire

protocol MainHubGetDataProviderProtocol {
    func getImages(_ page: Int, success: @escaping ([ImageModel]) -> Void)
}

class MainHubProvider {
    var endpoint = "https://picsum.photos/v2/list"
}

extension MainHubProvider: MainHubGetDataProviderProtocol {
    func getImages(_ page: Int, success: @escaping ([ImageModel]) -> Void) {
        AF.request(endpoint + "?page=\(page)&limit=100", method: .get, parameters: .none).response(completionHandler: {
            response in
            
            switch response.result {

            case .success(_):
                do {
                    let decoder = JSONDecoder()
                    let data = try decoder.decode([ImageModel].self, from: response.data ?? Data())
                    print(data)
                    
                    DispatchQueue.main.async {
                        success(data)
                    }
                }
                catch {
                    print("")
                }
            case .failure(let error):
                print(error)
            }
        })
    }
}

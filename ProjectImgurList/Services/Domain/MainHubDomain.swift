//
//  MainHubDomain.swift
//  ProjectImgurList
//
//  Created by Thiago dos Santos Martins on 16/03/22.
//

import Foundation

protocol MainHubGetDataInputProtocol: AnyObject {
    func getImages(_ page: Int)
}

protocol MainHubGetDataOutputProtocol: AnyObject {
    func getImagesSuccess(_ data: [ImageModel])
}

final class MainHubDomain {
    private var provider: MainHubGetDataProviderProtocol
    weak var response: MainHubGetDataOutputProtocol?
    
    init(provider: MainHubGetDataProviderProtocol) {
        self.provider = provider
    }
}

extension MainHubDomain: MainHubGetDataInputProtocol {
    func getImages(_ page: Int) {
        provider.getImages(page, success: {
            data in
            
            self.response?.getImagesSuccess(data)
        })
    }
}

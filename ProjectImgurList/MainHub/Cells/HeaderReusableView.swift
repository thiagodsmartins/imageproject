//
//  HeaderReusableView.swift
//  ProjectImgurList
//
//  Created by Thiago dos Santos Martins on 18/03/22.
//

import Foundation
import UIKit

class HeaderReusableView: UICollectionReusableView {
    var labelHeader: UILabel

    override init(frame: CGRect) {
        labelHeader = UILabel(frame: .zero)
        super.init(frame: .zero)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        labelHeader.numberOfLines = 0
        labelHeader.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(labelHeader)
        
        NSLayoutConstraint.activate([
            labelHeader.centerXAnchor.constraint(equalTo: centerXAnchor),
            labelHeader.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
        
        labelHeader.textColor = .black
        labelHeader.textAlignment = .center
        labelHeader.font = UIFont.systemFont(ofSize: 15.0)

        clipsToBounds = true
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.black.cgColor
    }
}

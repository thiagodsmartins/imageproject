//
//  ImgurImagesCell.swift
//  ProjectImgurList
//
//  Created by Thiago dos Santos Martins on 16/03/22.
//

import Foundation
import UIKit

class ImgurImagesCell: UICollectionViewCell {
    var imageViewTest: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        contentView.backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        return layoutAttributes
    }
    
    override func prepareForReuse() {
        imageViewTest.image = nil
    }
    
    private func setupViews() {
        imageViewTest = UIImageView()
        
        imageViewTest.contentMode = .scaleAspectFill
        imageViewTest.clipsToBounds = true
        imageViewTest.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(imageViewTest)
        
        constraints()
    }
    
    private func constraints() {
        NSLayoutConstraint.activate([
            imageViewTest.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageViewTest.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            imageViewTest.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            imageViewTest.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
    }
    
    func setImage(_ image: UIImage) {
        imageViewTest.image = image
    }
}

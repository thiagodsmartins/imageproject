//
//  MainHubSetup.swift
//  ProjectImgurList
//
//  Created by Thiago dos Santos Martins on 16/03/22.
//

import Foundation
import UIKit

extension MainHubViewController {
    func setupViews() {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        let collectionLayout = UICollectionViewFlowLayout()
        
        collectionLayout.scrollDirection = .vertical
        collectionLayout.sectionHeadersPinToVisibleBounds = true
        
        collectionView.register(ImgurImagesCell.self, forCellWithReuseIdentifier: "ImgurImagesCell")
        collectionView.register(HeaderReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderReusableView")
        collectionView.backgroundColor = .clear
        collectionView.setCollectionViewLayout(collectionLayout, animated: true)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        view.backgroundColor = .white
        view.addSubview(collectionView)
        
        setupConstraints()
    }
}

//
//  MainHubInteractor.swift
//  ProjectImgurList
//
//  Created by Thiago dos Santos Martins on 16/03/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import Foundation

final class MainHubInteractor {
    let imagesDomain: MainHubDomain
    weak var response: MainHubGetDataInteractorProtocol?
    
    init(imagesDomain: MainHubDomain) {
        self.imagesDomain = imagesDomain
    }
}

// MARK: - Extensions -

extension MainHubInteractor: MainHubInteractorInterface {
    func getImages(_ page: Int) {
        imagesDomain.getImages(page)
    }
}

extension MainHubInteractor: MainHubGetDataOutputProtocol {
    func getImagesSuccess(_ data: [ImageModel]) {
        response?.getImagesSucess(data)
    }
}
